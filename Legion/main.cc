#include <cstdio>
#include <cassert>
#include <cstdlib>
#include <string>
#include <vector>
#include "legion.h"

using namespace Legion;

#define TASK_COUNT 8

///////////////////////////////////////////////////////////////////////////
enum TaskIDs {
  TOP_LEVEL_TASK_ID,
  PI_CALC_TASK_ID,
};

///////////////////////////////////////////////////////////////////////////
unsigned long long get_n(const InputArgs &command_args)
{
    unsigned long long n = 10000;

    std::vector<std::string> vector_argv{command_args.argv, command_args.argv + command_args.argc};
    auto option = std::find(vector_argv.begin(), vector_argv.end(), "-n");

    if(option != vector_argv.end())
        n = std::stoull(option[1]);

    return n;
}

///////////////////////////////////////////////////////////////////////////
void top_level_task(
        const Task *task,
        const std::vector<PhysicalRegion> &regions,
        Context ctx,
        Runtime *runtime
    )
{
    // Retrieving how many iteration have to be done
    auto n = get_n(Runtime::get_input_args());
    auto n_per_task = n / TASK_COUNT;

    // Setting amount of tasks to be launched
    Rect<1> launch_bounds(0,TASK_COUNT-1);

    // Setting tasks arguments
    ArgumentMap arg_map;
    unsigned long long start = 0;
    for (int i = 0; i < TASK_COUNT; i++)
    {
        unsigned long long range[2] = {start, start + n_per_task};
        start += n_per_task;

        // Set range for task calculation
        arg_map.set_point(i, TaskArgument(range, sizeof(range)));
    }

    // Creating Task launcher with additonal "global" task argument
    double w = 1.0/n;
    IndexTaskLauncher index_launcher(
        PI_CALC_TASK_ID,
        launch_bounds,
        TaskArgument(&w, sizeof(w)),
        arg_map
    );

    // Running tasks and waiting for the results
    FutureMap fm = runtime->execute_index_space(ctx, index_launcher);
    fm.wait_all_results();

    // Reducing results to PI
    double pi = 0;
    for (int i = 0; i < TASK_COUNT; i++)
    {
        double partial_sum = fm.get_result<double>(i);
        pi += partial_sum;
    }

    pi *= w;

    printf("%.18f", pi);
}

///////////////////////////////////////////////////////////////////////////
double pi_calc_task(
        const Task *task,
        const std::vector<PhysicalRegion> &regions,
        Context ctx,
        Runtime *runtime
    )
{
    assert(task->index_point.get_dim() == 1);

    // Getting Argument form ArgumentMap
    assert(task->local_arglen == 2 * sizeof(unsigned long long));
    auto start = *((const unsigned long long*)task->local_args);
    auto end = *((const unsigned long long*)task->local_args + 1);

    // Getting Argument for TaskArgument
    assert(task->arglen == sizeof(double));
    double w = *(const double*)task->args;
    double sum = 0;
    double x;

    for (auto i = start; i < end; i++)
    {
        x = w * (i - 0.5);
        sum += 4.0 / (1.0 + x * x);
    }
    
    return sum;
}

///////////////////////////////////////////////////////////////////////////
int main(int argc, char **argv)
{
    Runtime::set_top_level_task_id(TOP_LEVEL_TASK_ID);

    // register top level task
    {
        TaskVariantRegistrar registrar(TOP_LEVEL_TASK_ID, "top_level");
        registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
        Runtime::preregister_task_variant<top_level_task>(registrar, "top_level");
    }

    // register pi_calc task
    {
        TaskVariantRegistrar registrar(PI_CALC_TASK_ID, "pi_calc_task");
        registrar.add_constraint(ProcessorConstraint(Processor::LOC_PROC));
        registrar.set_leaf();
        Runtime::preregister_task_variant<double, pi_calc_task>(registrar, "pi_calc_task");
    }

    return Runtime::start(argc, argv);
}