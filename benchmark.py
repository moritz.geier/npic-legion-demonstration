import time
import subprocess

def run_applications(iterations: str):
    print(f"Starting OpenMP with {iterations} iterations")
    start_time_openmp = time.time()
    subprocess.check_output(["./OpenMP/benchmark_builds/app_standard", "-n", iterations])
    openmp_time = time.time () - start_time_openmp
    print("OpenMP took ", openmp_time, "seconds")

    print(f"Starting Legion with {iterations} iterations")
    start_time_legion = time.time()
    subprocess.check_output(["./Legion/benchmark_builds/app_standard", "-ll:cpu", "8", "-n", iterations])
    legion_time = time.time () - start_time_legion
    print("Legion took ", legion_time, "seconds")
    return [openmp_time, legion_time]

def main():
    results_string: str = ""
    iterate_range = [1, 10, 100, 10_00, 10_000, 100_000, 1_000_000, 
                     10_000_000,  100_000_000,  1_000_000_000, 
                     10_000_000_000, 100_000_000_000, 
                     1_000_000_000_000]

    for i in iterate_range:
        results = run_applications(str(i))
        results_string += f"{i},{results[0]},{results[1]}\n"

    with open("benchmark_results.csv", "w") as csv:
        csv.write("Iterations,OpenMP,Legion\n")
        csv.write(results_string)



if "__main__" == __name__:
    main()