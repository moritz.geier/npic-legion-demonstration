# NPIC Seminar 2022: Task based programming with Legion

This repository should give a small comparison between OpenMP and Legion. I'm for no means a expert in either of the systems so take the benchmark result with a grain of salt.

## Getting Started
You can either run the `benchmark.py` script or compile the programms for yourself. To note is that Legion needs its runtime to be installed on the `LG_RT_DIR` enviroment variable set to be computed. OpenMP is compiled via CMake.
The `benchmark.py` script will output a CSV File with the results in it