#include <chrono>
#include <getopt.h>
#include <stdio.h>
#include <string>

unsigned long long get_n(int argc, char * const argv[])
{
    unsigned long long n = 10000;
    int opt = getopt(argc, argv, "n");

    if(opt == -1)
        return n;

    std::string number(argv[optind]);
    n = std::stoull (argv[optind]);

    return n;
}

///////////////////////////////////////////////////////////////////////////
int main(int argc, char * const argv[])
{
    auto n = get_n(argc, argv);

    double w = 1.0 / n;
    double sum = 0;
    double x;

    #pragma omp parallel for private(x), shared(w) reduction(+: sum)
    for (unsigned long long i = 0; i < n; i++)
    {
        x = w * (i - 0.5);
        sum += 4.0 / (1.0 + x * x);
    }

    double pi = w * sum;

    printf("%.18f", pi);
    return 0;
}
